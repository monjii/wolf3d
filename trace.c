/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   trace.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:34 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:35 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	move(t_env *e, char key)
{
	float	a;
	float	m_x;
	float	m_y;

	if (e->start_view >= 0.0 && e->start_view < (1.0 / 2.0 * PI))
	{
		a = e->start_view;
		m_x = (key == 'u' ? e->x + cosf(a) * 0.5 : e->x - cosf(a) * 0.5);
		m_y = (key == 'u' ? e->y - sinf(a) * 0.5 : e->y + sinf(a) * 0.5);
	}
	else if (e->start_view >= (1.0 / 2.0 * PI) && e->start_view < PI)
	{
		a = PI - e->start_view;
		m_x = (key == 'u' ? e->x - cosf(a) * 0.5 : e->x + cosf(a) * 0.5);
		m_y = (key == 'u' ? e->y - sinf(a) * 0.5 : e->y + sinf(a) * 0.5);
	}
	else if (e->start_view >= PI && e->start_view < 3.0 / 2.0 * PI)
	{
		printf("SO\n");
		a = e->start_view - PI;
		m_x = (key == 'u' ? e->x - cosf(a) * 0.5 : e->x + cosf(a) * 0.5);
		m_y = (key == 'u' ? e->y + sinf(a) * 0.5 : e->y - sinf(a) * 0.5);
	}
	else
	{
		printf("SE\n");
		a = 2.0 * PI - e->start_view;
		m_x = (key == 'u' ? e->x + cosf(a) * 0.5 : e->x - cosf(a) * 0.5);
		m_y = (key == 'u' ? e->y + sinf(a) * 0.5 : e->y - sinf(a) * 0.5);
	}
	printf("y %d x %d\n", (int)m_y, (int)m_x);
	if (m_x >= 0.0 && m_x < (float)e->map_len - 1.0 &&
		m_y >= 0.0 && m_y < (float)e->map_len - 1.0)
	{
		if (e->map[(int)m_y][(int)m_x] == 1)
			return ; 
		e->x = m_x;
		e->y = m_y;
	}
}

int		calculate_distance(t_env *e, float dx)
{
	float	dist;
	int		distfinal;
	float 	pos_x;
	float 	pos_y;

	pos_x = e->x;
	pos_y = e->y;
	dist = 0.0;
	if (dx >= 0.0 && dx < (1.0 / 2.0 * PI))
	{
		dist = north_east_ray(e, pos_x, pos_y, dx);
	}
	else if (dx >= (1.0 / 2.0 * PI) && dx < PI)
	{
		dist = north_west_ray(e, pos_x, pos_y, dx);
	}
	else if (dx >= PI && dx < 3.0 / 2.0 * PI)
		dist = south_west_ray(e, pos_x, pos_y, dx);
	else
		dist = south_east_ray(e, pos_x, pos_y, dx);
	//dist *= (cosf(dx - e->start_view));
	distfinal = (int)((float)SCR_Y / dist);
	return (distfinal);
}

void	draw_wall(t_env *e, int wall_size, int x)
{
	int		y;

	y = 0;
	if (wall_size > 0)
	{
		while (y < (SCR_Y - wall_size) / 2)
		{
			put_pixel_to_image(e, x, y, 0x6F, 0x98, 0xA2);
			y++;
		}
		while (y < ((SCR_Y - wall_size) / 2) + wall_size)
		{
			e->color == 'e' ? put_pixel_to_image(e, x, y, 0xF2, 0x83, 0x53) : 0;
			e->color == 's' ? put_pixel_to_image(e, x, y, 0xF3, 0x57, 0x30) : 0;
			e->color == 'o' ? put_pixel_to_image(e, x, y, 0xF2, 0x93, 0x53) : 0;
			e->color == 'n' ? put_pixel_to_image(e, x, y, 0xF2, 0x84, 0x68) : 0;
			y++;
		}
		while (y < SCR_Y)
		{
			put_pixel_to_image(e, x, y, 0xFF, 0xAE, 0x94);
			y++;
		}
	}
}

void	trace_rays(t_env *e)
{
	float	dx;
	int		x;
	int	wall_size;

	x = 0;
	dx = 0.0;
	while (x < SCR_X)
	{
		dx = (e->start_view + e->fov / 2.0) - (e->fov / 800.0 * (float)x);
		(dx > 2.0 * PI) ? dx -= 2.0 * PI : 0.0;
		(dx < 0.0) ? dx += 2.0 * PI : 0.0;
		wall_size = calculate_distance(e, dx);
		draw_wall(e, wall_size, x);		
		x++;
	}
}
