/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgirard <jgirard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/15 01:15:54 by jgirard           #+#    #+#             */
/*   Updated: 2014/01/15 01:46:43 by jgirard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *s1, const void *s2, size_t n)
{
	char		*dest;
	const char	*src;
	size_t		i;

	dest = s1;
	src = s2;
	i = n - 1;
	if (s2 <= s1)
	{
		while (n > 0)
		{
			dest[i] = src[i];
			i--;
			n--;
		}
	}
	else
		ft_memcpy(dest, src, n);
	return (dest);
}
