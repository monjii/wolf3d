/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgirard <jgirard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/15 01:26:30 by jgirard           #+#    #+#             */
/*   Updated: 2014/01/15 01:26:31 by jgirard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	int		i;
	int		end;
	int		new;
	char	*dest;

	i = 0;
	end = ft_strlen(s);
	new = 0;
	dest = (char *)malloc(sizeof(char*) * (ft_strlen(s) + 1));
	while (s[i] == ' ' || s[i] == '\t' || s[i] == '\n')
		i++;
	while (s[end] == '\0' || s[end] == ' ' || s[end] == '\t' || s[end] == '\n')
		end--;
	while (i <= end)
	{
		dest[new] = s[i];
		new++;
		i++;
	}
	dest[new] = '\0';
	return (dest);
}
