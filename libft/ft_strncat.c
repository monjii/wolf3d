/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgirard <jgirard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/15 01:23:15 by jgirard           #+#    #+#             */
/*   Updated: 2014/01/15 01:23:16 by jgirard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncat(char *s1, const char *s2, size_t n)
{
	int		i;
	int		cnt;

	i = ft_strlen(s1);
	cnt = 0;
	while (s2[cnt] != '\0' && n > 0)
	{
		s1[i] = s2[cnt];
		i++;
		cnt++;
		n--;
	}
	s1[i] = '\0';
	return (s1);
}
