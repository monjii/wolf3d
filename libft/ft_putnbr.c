/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgirard <jgirard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/15 01:17:03 by jgirard           #+#    #+#             */
/*   Updated: 2014/01/15 01:47:24 by jgirard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void		ft_putnbr_neg(int n)
{
	if (n <= 9)
		ft_putchar(n + '0');
	else
	{
		ft_putnbr_neg(n / 10);
		ft_putchar(n % 10 + '0');
	}
}

void			ft_putnbr(int n)
{
	if (n < 0)
	{
		ft_putchar('-');
		ft_putnbr(-n);
	}
	else
		ft_putnbr_neg(n);
}
