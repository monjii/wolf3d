/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   aff_rev_list_str.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgirard <jgirard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/15 01:10:55 by jgirard           #+#    #+#             */
/*   Updated: 2014/01/15 01:10:55 by jgirard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	aff_rev_list_str(t_list *new)
{
	if (new == NULL)
		return ;
	else
		aff_rev_list_str(new->next);
	ft_putendl(new->content);
}
