/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgirard <jgirard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/15 01:25:49 by jgirard           #+#    #+#             */
/*   Updated: 2014/01/15 01:25:49 by jgirard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	while (*s != '\0')
		s++;
	s--;
	if (c == 0)
		s++;
	while (*s != c)
	{
		if (*s == '\0')
			return (NULL);
		s--;
	}
	return ((char *)s);
}
