/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgirard <jgirard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/15 01:14:59 by jgirard           #+#    #+#             */
/*   Updated: 2014/04/15 13:08:10 by jgirard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *s1, const void *s2, int c, size_t n)
{
	char			*dest;
	char			*src;
	unsigned char	chr;

	dest = (char *)s1;
	src = (char *)s2;
	chr = (unsigned char)c;
	while (n)
	{
		if ((*dest = *src) == chr)
			return (++dest);
		n--;
		dest++;
		src++;
	}
	return (NULL);
}
