/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgirard <jgirard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/15 01:22:35 by jgirard           #+#    #+#             */
/*   Updated: 2014/01/15 01:22:36 by jgirard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	const char	*src2;
	size_t		size2;
	size_t		len;
	char		*dst2;

	len = size;
	dst2 = dst;
	src2 = src;
	size2 = size;
	while (*dst2 != '\0' && size2-- > 0)
		dst2++;
	len = dst2 - dst;
	size2 = size - len;
	if (size2 == 0)
		return (len + ft_strlen(src2));
	while (*src2 != '\0')
	{
		if (size2-- > 1)
		{
			*dst2++ = *src2;
		}
		src2++;
	}
	*dst2 = '\0';
	return (len + (src2 - src));
}
