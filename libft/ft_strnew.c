/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgirard <jgirard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/15 01:24:02 by jgirard           #+#    #+#             */
/*   Updated: 2014/01/15 01:24:17 by jgirard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnew(size_t size)
{
	char	*dest;

	dest = (char *)malloc(sizeof(char) * (size + 1));
	if (dest)
	{
		ft_bzero(dest, (size + 1));
		return (dest);
	}
	return (NULL);
}
