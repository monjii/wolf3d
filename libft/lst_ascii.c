/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_ascii.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgirard <jgirard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/15 01:28:34 by jgirard           #+#    #+#             */
/*   Updated: 2014/01/15 01:30:10 by jgirard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*cons(char *src, t_list *first)
{
	t_list		*tmp;

	tmp = ft_lstnew(src, ft_strlen(src));
	tmp->content = src;
	tmp->next = first;
	return (tmp);
}

t_list		*inser_tri(char *s1, t_list *first)
{
	if (first == NULL)
		return (cons(s1, first));
	else if (ft_strcmp(s1, first->content) < 0)
		return (cons(s1, first));
	else
		first->next = inser_tri(s1, first->next);
	return (first);
}
