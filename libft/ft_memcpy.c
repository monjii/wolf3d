/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgirard <jgirard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/15 01:15:37 by jgirard           #+#    #+#             */
/*   Updated: 2014/04/15 13:09:54 by jgirard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *s1, const void *s2, size_t n)
{
	int			i;
	char		*s1b;
	const char	*s2b;

	i = 0;
	s1b = s1;
	s2b = s2;
	while (n > 0)
	{
		s1b[i] = s2b[i];
		n--;
		i++;
	}
	return (s1b);
}
