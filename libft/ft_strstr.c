/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jgirard <jgirard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/15 01:26:15 by jgirard           #+#    #+#             */
/*   Updated: 2014/01/15 01:48:39 by jgirard          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char		*ft_resostr(const char *s1, const char *s2)
{
	int			i;
	int			cnt;
	char		*tmp;

	i = 0;
	cnt = 0;
	while (s1[i] != 0)
	{
		while (s1[i] == s2[cnt])
		{
			i++;
			cnt++;
			if (s2[cnt] == 0)
			{
				tmp = (char *)&s1[i - cnt];
				return (tmp);
			}
		}
		cnt = 0;
		i++;
	}
	return (NULL);
}

char			*ft_strstr(const char *s1, const char *s2)
{
	char		*tmp;

	if (*s2 == '\0')
		tmp = (char *)s1;
	else
		tmp = ft_resostr(s1, s2);
	return (tmp);
}
