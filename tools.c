/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:37:40 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:37:42 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	usage(char *error)
{
	ft_putendl(error);
	exit(0);
}

void	quit_program(char *str, t_env *e)
{
	mlx_destroy_image(e->mlx_ptr, e->img_ptr);
	mlx_destroy_window(e->mlx_ptr, e->win_ptr);
	usage(str);
}

int		count_y(t_env *e, char *line)
{
	int		i;

	i = 0;
	while (get_next_line(e->fd, &line))
	{
		free(line);
		i++;
	}
	return (i);
}
/*
void	print_board(t_env *e)
{
	int i;
	int j;

	j = 0;
	while (j < e->map_len)
	{
		i = 0;
		while (i < e->map_len)
		{
			printf("%d ", e->map[j][i]);
			i++;
		}
		printf("\n");
		j++;
	}
}*/

void	parser(int argc, char **argv, t_env *e)
{
	if (argc != 2)
		usage("Please link the map like below :\n    ./wolf3d map.txt\n");
	open_map(e, argv[1]);
	parse_map(e);
	close(e->fd);
}

void	put_pixel_to_image(t_env *e, int x, int y, int r, int g, int b)
{
	int		index;

	index = y * e->sl + x * 4;
	e->img_data[index] = b;
	e->img_data[index + 1] = g;
	e->img_data[index + 2] = r;
}
