/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <jfuster@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/05 14:06:44 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/05 14:06:46 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		n_values(char *line)
{
	char	**new_line;
	int		i;

	new_line = ft_strsplit(line, ' ');
	i = 0;
	while (new_line[i] != NULL)
		i++;
	return (i);
}

void	open_map(t_env *e, char *path)
{
	int		fd;

	if ((fd = open(path, O_RDONLY)) < 0)
		usage("Opening error");
	e->fd = fd;
	e->path = ft_strdup(path);
	if (read(fd, (char *)0, 0) < 0)
		usage("Opening error");
}

void	check_line_chars(char *line)
{
	int		i;

	i = 0;
	if (line[i] == '\0')
		usage("Empty line found");
	while (line[i] != '\0')
	{
		if (line[i] != '1' && line[i] != '0' && line[i] != ' ')
			usage("Invalid character found");
		i++;
	}
}

void	parse_map(t_env *e)
{
	char	*line;
	int		first;
	int		y;

	e->map_len = count_y(e, line);
	close(e->fd);
	e->fd = open(e->path, O_RDONLY);
	e->map = (int **)malloc(sizeof(int *) * e->map_len);
	y = 0;
	while (y < e->map_len)
	{
		e->map[y] = (int *)malloc(sizeof(int) * e->map_len);
		y++;
	}
	y = 0;
	while (get_next_line(e->fd, &line))
	{
		check_line_chars(line);
		if (n_values(line) != e->map_len)
			usage("Line length error");
		store_lines_value(e, line, y);
		free(line);
		y++;
	}
}

void	store_lines_value(t_env *e, char *line, int y)
{
	char	**new_line;
	int		x;

	new_line = ft_strsplit(line, ' ');
	x = 0;
	while (new_line[x] != NULL)
	{
		e->map[y][x] = mdfy_atoi(new_line[x]);
		x++;
	}
}
