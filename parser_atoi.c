/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_atoi.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <jfuster@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/05 14:06:54 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/05 14:06:55 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		mdfy_atoi(const char *str)
{
	int		i;
	int		compt;
	int		min;
	int		valid;

	i = 0;
	compt = 0;
	min = 0;
	valid = 0;
	if (str[i] == '-' || str[i] == '+')
		i++;
	if (str[i] == '-')
		min++;
	while (ft_isdigit(str[i]))
	{
		valid = 1;
		compt = compt * 10;
		compt = compt + str[i] - '0';
		i++;
	}
	if (!valid || i != ft_strlen(str))
		usage("Invalid number found");
	if (min > 0)
		return (-compt);
	return (compt);
}
