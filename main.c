/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:38:03 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:38:06 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		expose_hook(t_env *e)
{
	trace_rays(e);
	mlx_put_image_to_window(e->mlx_ptr, e->win_ptr, e->img_ptr, 0, 0);
	return (0);
}

int		key_hook(int keycode, t_env *e)
{
	if (keycode == KEY_ESC)
		quit_program("End of program", e);
	if (keycode == KEY_LEFT)
		e->start_view += 0.1;
	if (keycode == KEY_RIGHT)
		e->start_view -= 0.1;
	e->start_view < 0.0 ? e->start_view += 2.0 * PI : 0;
	e->start_view > 2.0 * PI ? e->start_view -= 2.0 * PI : 0;
	if (keycode == KEY_UP)
		move(e, 'u');
	if (keycode == KEY_DOWN)
		move(e, 'd');
	trace_rays(e);
	mlx_put_image_to_window(e->mlx_ptr, e->win_ptr, e->img_ptr, 0, 0);
	return (0);
}

int		main(int argc, char **argv)
{
	t_env	*e;

	e = (t_env *)malloc(sizeof(t_env));
	parser(argc, argv, e);
	//print_board(e);
	if ((e->mlx_ptr = mlx_init()) == NULL)
		return (1);
	if ((e->win_ptr = mlx_new_window(e->mlx_ptr, SCR_X, SCR_Y, "wolf3d"))
		== NULL)
		return (1);
	e->img_ptr = mlx_new_image(e->mlx_ptr, SCR_X, SCR_Y);
	e->img_data = mlx_get_data_addr(e->img_ptr, &(e->bpp), &(e->sl), &(e->end));
	e->x = 4.8;
	e->y = 4.5;
	e->start_view = 1.0 / 2.0 * PI;
	e->fov = 1.0 / 2.0 * PI;
	e->where = 0;
	mlx_expose_hook(e->win_ptr, expose_hook, e);
	mlx_key_hook(e->win_ptr, key_hook, e);
	mlx_loop(e->mlx_ptr);
	return (0);
}
