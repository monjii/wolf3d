NAME = wolf3d

FLAGS = -Wall -Wextra -Werror

LIBFT = -L./libft/ -lft

MLX = -L./minilibX/ -lmlx -framework OpenGL -framework AppKit

SRCS = main.c tools.c parser.c parser_atoi.c trace.c rays.c

SRCO = $(SRCS:%.c=%.o)

all: $(NAME)

$(NAME): $(SRCO)
	make -C libft/
	gcc -o $(NAME) $(FLAGS) $(LIBFT) $(MLX) $(SRCO)

$(SRCO):
	gcc -c $(SRCS) $<

clean:
	make -C libft/ clean
	/bin/rm -f $(SRCO)

fclean: clean
	make -C libft/ fclean
	/bin/rm -f $(NAME)

re: fclean all

mlx:
	make -C minilibX/
