/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rays.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 14:06:49 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 14:06:51 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

float	north_east_ray(t_env *e, float pos_x, float pos_y, float dx)
{
	float a;
	float b;
	float h;
	float dist_h;
	float angle;

	angle = dx;
	dist_h = MAX_DIST_VIEW;
	while ((int)pos_x < (e->map_len))
	{
		a = (float)((int)(pos_x + 1.0) - e->x);
		h = a / cosf(angle);
		b = sinf(angle) * h;
		if ((int)(pos_y - b) >= 0 && e->map[(int)(pos_y - b)][(int)pos_x + 1] == 1)
		{
			dist_h = h;
			e->color = 'o';
			break ;
			
		}
		pos_x += 1.0;
	}
	pos_x = e->x;
	angle = 1.0 / 2.0 * PI - dx;
	while ((int)pos_y > 0 && pos_y > 0.0)
	{
		a = e->y - (float)((int)pos_y);
		h = a / cosf(angle);
		b = sinf(angle) * h;
		if ((int)(pos_x + b) < (e->map_len) && e->map[(int)pos_y - 1][(int)(pos_x + b)] == 1 && h < dist_h)
		{
			dist_h = h;
			e->color = 's';
			break ;
		}
		pos_y -= 1.0;
	}
	return (dist_h);
}

float	north_west_ray(t_env *e, float pos_x, float pos_y, float dx)
{
	float a;
	float b;
	float h;
	float dist_h;
	float angle;

	angle = PI - dx;
	dist_h =  MAX_DIST_VIEW;
	while ((int)pos_x > 0 && pos_x > 0.0)
	{
		a = e->x - (float)((int)pos_x);
		h = a / cosf(angle);
		b = sinf(angle) * h;
		if ((int)(pos_y - b) >= 0 && e->map[(int)(pos_y - b)][(int)pos_x - 1] == 1)
		{
			dist_h = h;
			e->color = 'e';
			break ;
			
		}
		pos_x -= 1.0;
	}
	pos_x = e->x;
	angle = dx - (1.0 / 2.0 * PI);
	while ((int)pos_y > 0 && pos_y > 0.0)
	{
		a = e->y - (float)((int)pos_y);
		h = a / cosf(angle);
		b = sinf(angle) * h;
		if ((int)(pos_x - b) >= 0 && e->map[(int)pos_y - 1][(int)(pos_x - b)] == 1 && h < dist_h)
		{
			dist_h = h;
			e->color = 's';
			break ;
		}
		pos_y -= 1.0;
	}
	return (dist_h);
}

float	south_west_ray(t_env *e, float pos_x, float pos_y, float dx)
{
	float a;
	float b;
	float h;
	float dist_h;
	float angle;

	angle = dx - PI;
	dist_h =  MAX_DIST_VIEW;
	while ((int)pos_x > 0 && pos_x > 0.0)
	{
		a = e->x - (float)((int)pos_x);
		h = a / cosf(angle);
		b = sinf(angle) * h;
		if ((int)(pos_y + b) < (e->map_len) && e->map[(int)(pos_y + b)][(int)pos_x - 1] == 1)
		{
			dist_h = h;
			e->color = 'e';
			break ;
			
		}
		pos_x -= 1.0;
	}
	pos_x = e->x;
	angle = 3.0 / 2.0 * PI - dx;
	while ((int)pos_y < (e->map_len) - 1)
	{
		a = (float)((int)(pos_y + 1.0) - e->y);
		h = a / cosf(angle);
		b = sinf(angle) * h;
		if ((int)(pos_x - b) >= 0 && e->map[(int)pos_y + 1][(int)(pos_x - b)] == 1 && h < dist_h)
		{
			dist_h = h;
			e->color = 'n';
			break ;
		}
		pos_y += 1.0;
	}
	return (dist_h);
}

float	south_east_ray(t_env *e, float pos_x, float pos_y, float dx)
{
	float a;
	float b;
	float h;
	float dist_h;
	float angle;

	angle = 2.0 * PI - dx;
	dist_h =  MAX_DIST_VIEW;
	while ((int)pos_x < (e->map_len))
	{
		a = (float)((int)(pos_x + 1.0) - e->x);
		h = a / cosf(angle);
		b = sinf(angle) * h;
		if ((int)(pos_y + b) < (e->map_len) && e->map[(int)(pos_y + b)][(int)pos_x + 1] == 1)
		{
			dist_h = h;
			e->color = 'o';
			break ;
		}
		pos_x += 1.0;
	}
	pos_x = e->x;
	angle = dx - 3.0 / 2.0 * PI;
	while ((int)pos_y < (e->map_len) - 1)
	{
		a = (float)((int)(pos_y + 1.0) - e->y);
		h = a / cosf(angle);
		b = sinf(angle) * h;
		if ((int)(pos_x + b) < (e->map_len) && e->map[(int)pos_y + 1][(int)(pos_x + b)] == 1 && h < dist_h)
		{
			dist_h = h;
			e->color = 'n';
			break ;
		}
		pos_y += 1.0;
	}
	return (dist_h);
}
