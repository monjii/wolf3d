/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfuster <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/18 11:37:53 by jfuster           #+#    #+#             */
/*   Updated: 2016/01/18 11:37:55 by jfuster          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H_
# define FDF_H_
# include "minilibX/mlx.h"
# include "libft/libft.h"
# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include <fcntl.h>
# include <math.h>

# define MAX_DIST_VIEW 99999.0
# define PI 3.14159265
# define KEY_ESC 53
# define KEY_LEFT 123
# define KEY_RIGHT 124
# define KEY_UP 126
# define KEY_DOWN 125
# define SCR_X 800
# define SCR_Y 550

typedef struct	s_env
{
	int			map_len;
	char		*path;
	int			fd;
	void		*img_ptr;
	char		*img_data;
	void		*mlx_ptr;
	void		*win_ptr;

	int			sl;
	int			bpp;
	int			end;

	int			**map;
	float		fov;
	float		x;
	float		y;
	float		start_view;

	int			where;
	char 		color;

}				t_env;

/*
**	main.c
*/
int				expose_hook(t_env *e);
int				key_hook(int keycode, t_env *e);

/*
**	parser.c  parser_atoi.c
*/
int				n_values(char *line);
void			open_map(t_env *e, char *path);
void			check_line_chars(char *line);
void			parse_map(t_env *e);
void			store_lines_value(t_env *e, char *line, int y);
int				mdfy_atoi(const char *str);

/*
**	tools.c
*/
void			usage(char *error);
void			quit_program(char *str, t_env *e);
void			parser(int argc, char **argv, t_env *e);
int				count_y(t_env *e, char *line);
void			put_pixel_to_image(t_env *e, int x, int y, int r, int g, int b);
//void			print_board(t_env *e);

/*
**	trace.c
*/
void			move(t_env *e, char key);
int				calculate_distance(t_env *e, float dx);
void			draw_wall(t_env *e, int wall_size, int x);
void			trace_rays(t_env *e);

/*
**	rays.c
*/
float			north_east_ray(t_env *e, float pos_x, float pos_y, float dx);
float			north_west_ray(t_env *e, float pos_x, float pos_y, float dx);
float			south_west_ray(t_env *e, float pos_x, float pos_y, float dx);
float			south_east_ray(t_env *e, float pos_x, float pos_y, float dx);

#endif
